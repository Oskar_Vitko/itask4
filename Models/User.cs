﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITask_4.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime RegisterDate { get; set; }

        public DateTime LastActiveDate { get; set; }
        public bool isBlocked { get; set; }
    }
}