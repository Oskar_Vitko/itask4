﻿using ITask_4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITask_4.Repository
{
    public class UserRepository
    {
        private static UserContext db = new UserContext();

        public static User GetUserByEmail(string email)
        {
            User user = null;
            user = db.Users.FirstOrDefault(u => u.Email == email);
            return user;
        }
        public static bool Add(string email, string password, string name)
        {
            if(GetUserByEmail(email) == null)
            {
                db.Users.Add(new User() {
                    Email = email,
                    Name = name,
                    Password = password,
                    RegisterDate = DateTime.Now,
                    LastActiveDate = DateTime.Now,
                    isBlocked = false
                });
                db.SaveChanges();
                return true;
            }
            return false;
        }
        public static void Block(List<int> ids)
        {
            foreach (int i in ids)
            {
                db.Users.FirstOrDefault(u => u.Id == i).isBlocked = true;
                db.SaveChanges();
            }
        }

        public static void UnBlock(List<int> ids)
        {
            foreach (int i in ids)
            {
                db.Users.FirstOrDefault(u => u.Id == i).isBlocked = false;
                db.SaveChanges();
            }
        }

        public static void Delete(List<int> ids)
        {
            foreach (int i in ids)
            {
                db.Users.Remove(db.Users.FirstOrDefault(u => u.Id == i));
                db.SaveChanges();
            }
        }
    }
}