﻿using System;
using ITask_4.Models;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ITask_4.Repository;
using System.Linq;
using System.Data.Entity;

namespace ITask_4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Table()
        {
            User user = null;
            if (User.Identity.IsAuthenticated)
            {
                UserContext db = new UserContext();
                user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
                if (user != null && !user.isBlocked)
                    return View(db.Users.ToList());
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult Table(FormCollection collection)
        {
            if (!string.IsNullOrEmpty(collection["chkbox"]))
            {
                string[] checkboxes = checkboxes = collection["chkbox"].ToString().Split(',');
                List<int> ids = new List<int>();
                foreach (var box in checkboxes)
                    ids.Add(Convert.ToInt32(box));
                if (collection.AllKeys[0] == "block") UserRepository.Block(ids);
                else if (collection.AllKeys[0] == "unblock") UserRepository.UnBlock(ids);
                else if (collection.AllKeys[0] == "delete") UserRepository.Delete(ids);
            }
            return RedirectToAction("Table", "Home");
        }
    }
}