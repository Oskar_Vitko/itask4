﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ITask_4.Models;
using ITask_4.Repository;

namespace ITask_4.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = UserRepository.GetUserByEmail(model.Email);
                if (user != null)
                {
                    if (!user.isBlocked)
                    {
                        FormsAuthentication.SetAuthCookie(model.Email, true);
                        return RedirectToAction("Table", "Home");
                    }
                    else ModelState.AddModelError("", "Данный аккаунт заблокирован");
                }
                else ModelState.AddModelError("", "Неверный логин и(или) пароль");
            }
            return View(model);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserRepository.Add(model.Email, model.Password, model.Name))
                {
                    FormsAuthentication.SetAuthCookie(model.Email, true);
                    return RedirectToAction("Table", "Home");
                }
                else ModelState.AddModelError("", "Пользователь с таким логином уже существует");
            }
            return View(model);
        }

        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            User user = UserRepository.GetUserByEmail(User.Identity.Name);
            if (user != null)
                user.LastActiveDate = DateTime.Now;
            return RedirectToAction("Login", "Account");
        }
    }
}